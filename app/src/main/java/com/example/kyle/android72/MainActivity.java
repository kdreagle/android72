package com.example.kyle.android72;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    static HashMap<String, ImageView> cells;

    static GridLayout board;

    static TextView status;

    static TextView turn;

    static Chess thisTurn;

    static ImageView src, dest;

    static Button undo;

    static Button autoMove;

    static Button saveGame;

    static Button goBack;

    static Button resign;

    static Button draw;

    static Drawable undoSRC;

    static Drawable bPawn;
    static Drawable wPawn;
    static Drawable bKing;
    static Drawable bQueen;
    static Drawable bRook;
    static Drawable bKnight;
    static Drawable bBishop;
    static Drawable wQueen;
    static Drawable wRook;
    static Drawable wKnight;
    static Drawable wBishop;
    static Drawable wKing;

    static boolean isDraw;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeBoard();



        saveGame = (Button) findViewById(R.id.saveGame);
        saveGame.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, RequestSave.class);
                i.putExtra("moves",new ArrayList<>(thisTurn.moveOrder));
                startActivity(i);
            }
        });
    }

    public void goHome(View v){
        finish();
    }

    void draw(View v) {
        if (isDraw) {

           turn.setText("Draw!");
           status.setText("");
           thisTurn.moveOrder.add("dr12");
            saveGame.setVisibility(View.VISIBLE);
            undo.setVisibility(View.INVISIBLE);
            autoMove.setVisibility(View.INVISIBLE);
            draw.setVisibility(View.INVISIBLE);
            resign.setVisibility(View.INVISIBLE);
        } else {
            isDraw = true;
            status.setText("Draw?");
        }
    }

    void resign(View v) {

        if (thisTurn.whitesTurn) {
            turn.setText("Black Wins!");
            thisTurn.moveOrder.add("bw12");
        } else {
            thisTurn.moveOrder.add("ww12");
            turn.setText("White Wins!");
        }
        saveGame.setVisibility(View.VISIBLE);
        undo.setVisibility(View.INVISIBLE);
        autoMove.setVisibility(View.INVISIBLE);
        draw.setVisibility(View.INVISIBLE);
        resign.setVisibility(View.INVISIBLE);
    }



    void initializeBoard() {

        thisTurn = new Chess();

        thisTurn.initializeBoard();

        undo = (Button) findViewById(R.id.undo);

        autoMove = (Button) findViewById(R.id.autoMove);

        resign = (Button) findViewById(R.id.resign);

        draw = (Button) findViewById(R.id.draw);

        goBack = (Button) findViewById(R.id.goBack);

        board = (GridLayout) findViewById(R.id.board);

        status = (TextView) findViewById(R.id.status);

        turn = (TextView) findViewById(R.id.turn);

        autoMove.setOnClickListener(new AutoMoveClickListener());

        undo.setOnClickListener(new UndoClickListener());

        src = null;
        dest = null;

        isDraw = false;

        ImageView cell;

        cells = new HashMap<String, ImageView>();

        for (int rank = 8; rank > 0 ; rank--) {
            for (int file = 'a'; file <= 'h'; file++) {

                String position = "" + (char) file + rank;
                cell = new ImageView(this);


                if (rank == 7) {
                    cell.setImageResource(R.drawable.ic_bp);
                    bPawn = cell.getDrawable();
                } else if (rank == 2) {
                    cell.setImageResource(R.drawable.ic_wp);
                    wPawn = cell.getDrawable();
                } else if (position.equals("a8") || position.equals("h8")) {
                    cell.setImageResource(R.drawable.ic_br);
                    bRook = cell.getDrawable();
                } else if (position.equals("b8") || position.equals("g8")) {
                    cell.setImageResource(R.drawable.ic_bn);
                    bKnight = cell.getDrawable();
                } else if (position.equals("c8") || position.equals("f8")) {
                    cell.setImageResource(R.drawable.ic_bb);
                    bBishop  = cell.getDrawable();
                } else if (position.equals("d8")) {
                    cell.setImageResource(R.drawable.ic_bq);
                    bQueen  = cell.getDrawable();
                } else if (position.equals("e8")) {
                    cell.setImageResource(R.drawable.ic_bk);
                    bKing  = cell.getDrawable();
                } else if (position.equals("a1") || position.equals("h1")) {
                    cell.setImageResource(R.drawable.ic_wr);
                    wRook  = cell.getDrawable();
                } else if (position.equals("b1") || position.equals("g1")) {
                    cell.setImageResource(R.drawable.ic_wn);
                    wKnight  = cell.getDrawable();
                } else if (position.equals("c1") || position.equals("f1")){
                    cell.setImageResource(R.drawable.ic_wb);
                    wBishop  = cell.getDrawable();
                } else if (position.equals("d1")){
                    cell.setImageResource(R.drawable.ic_wq);
                    wQueen  = cell.getDrawable();
                } else if (position.equals("e1")) {
                    cell.setImageResource(R.drawable.ic_wk);
                    wKing  = cell.getDrawable();
                }


                cell.setMinimumWidth(92);
                cell.setMinimumHeight(92);


                cell.setTag(position);
                cells.put(position, cell);


                cell.setOnClickListener(new CellClickListener());



                board.addView(cell);
            }
        }
        ImageView c = new ImageView(this);
        c.setImageResource(R.drawable.ic_bb);
    }
    public static void updateBoard() {

        Map<String, ChessPiece> curBoard = thisTurn.board;

        ImageView cell;
        HashMap<String, ImageView> cells = MainActivity.cells;

        for (String position : cells.keySet()) {
            cell = cells.get(position);
            if (curBoard.containsKey(position)) {
                ChessPiece chessPiece = curBoard.get(position);
                char color = chessPiece.toString().charAt(0);
                char piece = chessPiece.toString().charAt(1);
                if (piece == 'P' && color == 'b')
                    cell.setImageDrawable(MainActivity.bPawn);
                else if (piece == 'P' && color == 'w')
                    cell.setImageDrawable(MainActivity.wPawn);
                else if (piece == 'R' && color == 'b')
                    cell.setImageDrawable(MainActivity.bRook);
                else if (piece == 'N' && color == 'b')
                    cell.setImageDrawable(MainActivity.bKnight);
                else if (piece == 'B' && color == 'b')
                    cell.setImageDrawable(MainActivity.bBishop);
                else if (piece == 'Q' && color == 'b')
                    cell.setImageDrawable(MainActivity.bQueen);
                else if (piece == 'K' && color == 'b')
                    cell.setImageDrawable(MainActivity.bKing);
                else if (piece == 'R' && color == 'w')
                    cell.setImageDrawable(MainActivity.wRook);
                else if (piece == 'N' && color == 'w')
                    cell.setImageDrawable(MainActivity.wKnight);
                else if (piece == 'B' && color == 'w')
                    cell.setImageDrawable(MainActivity.wBishop);
                else if (piece == 'Q' && color == 'w')
                    cell.setImageDrawable(MainActivity.wQueen);
                else if (piece == 'K' && color == 'w')
                    cell.setImageDrawable(MainActivity.wKing);
            } else {
                cell.setImageDrawable(null);
            }
        }
    }
}
