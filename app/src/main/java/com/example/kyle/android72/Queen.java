package com.example.kyle.android72;

/**
 * @author Matthew Skrobola, Kyle Reagle
 *
 */
public class Queen extends ChessPiece {
	
	/** 
	 * Queen chess piece
	 * @param color color of the queen
	 * @param location location of queen
	 */
	public Queen(String color, String location) {
		super(color,location);
	}
	
	/** 
	 * @return the correct notation for printing to the board
	 */
	public String toString() {
		return getColor()+"Q";
	}
}
