package com.example.kyle.android72;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

/**
 * Created by mattskrobola on 12/12/17.
 */

public class ViewGames extends AppCompatActivity {
    boolean flipName = true;
    boolean flipDate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_games);
        final ListView listView =  (ListView) findViewById(R.id.listview);

        ArrayList<SavedGame> savedGames = new ArrayList<>();
        ObjectInputStream input;
        String filename = "SavedGames";



        try {
            input = new ObjectInputStream(new FileInputStream(new File(new File(getFilesDir(),"")+File.separator+filename)));
            savedGames = (ArrayList<SavedGame>) input.readObject();
            input.close();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        final ArrayList<SavedGame> test = savedGames;


        final ArrayAdapter<SavedGame> arrayAdapter = new ArrayAdapter<SavedGame>(
                this,
                android.R.layout.simple_list_item_1,
                savedGames );
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(final AdapterView<?> myAdapter, View myView, final int myItemInt, long mylng) {
                Button confirm = findViewById(R.id.confirm);
                confirm.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {

                        SavedGame cur = (SavedGame) arrayAdapter.getItem(myItemInt);
                        Intent i = new Intent(ViewGames.this, PlayOldGame.class);

                        i.putExtra("moves",new ArrayList<String>(cur.moves));
                        startActivity(i);
                        finish();
                    }
                });

            }
        });
        Button sortByName = findViewById(R.id.sortByName);
        sortByName.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                for(int i =0; i < test.size() -1 ;i++){
                    SavedGame tempGame = test.get(i);
                    int tempIndex = i;
                    for(int j = i+1; j < test.size(); j++){
                        if(flipName) {
                            if (tempGame.title.toLowerCase().compareTo(test.get(j).title.toLowerCase()) > 0) {
                                tempGame = test.get(j);
                                tempIndex = j;
                            }
                        } else {
                            if (tempGame.title.toLowerCase().compareTo(test.get(j).title.toLowerCase()) < 0) {
                                tempGame = test.get(j);
                                tempIndex = j;
                            }
                        }
                    }
                    test.set(tempIndex, test.get(i));
                    test.set(i, tempGame);
                }
                arrayAdapter.notifyDataSetChanged();

                flipName = !flipName;


            }
        });
        Button sortByDate = findViewById(R.id.sortByDate);
        sortByDate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                for(int i =0; i < test.size() -1 ;i++){
                    SavedGame tempGame = test.get(i);
                    int tempIndex = i;
                    for(int j = i+1; j < test.size(); j++){
                        if(flipDate) {
                            if (tempGame.addedDate.compareTo(test.get(j).addedDate) > 0) {
                                tempGame = test.get(j);
                                tempIndex = j;
                            }
                        } else {
                            if (tempGame.addedDate.compareTo(test.get(j).addedDate) < 0) {
                                tempGame = test.get(j);
                                tempIndex = j;
                            }
                        }
                    }
                    test.set(tempIndex, test.get(i));
                    test.set(i, tempGame);
                }
                arrayAdapter.notifyDataSetChanged();
                flipDate = !flipDate;

            }
        });
    }




    public void sortByDate(){


    }


}
