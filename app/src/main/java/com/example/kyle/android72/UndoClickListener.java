package com.example.kyle.android72;

import android.view.View;

/**
 * Created by Kyle on 12/5/2017.
 */

class UndoClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        if(MainActivity.thisTurn.undo()){

            MainActivity.updateBoard();
            MainActivity.thisTurn.analyzeBoard();
            if (MainActivity.thisTurn.whitesTurn) {
                MainActivity.thisTurn.whitesTurn = false;
                MainActivity.turn.setText("Black's turn");
            } else {
                MainActivity.thisTurn.whitesTurn = true;
                MainActivity.turn.setText("White's turn");
            }

            if (MainActivity.thisTurn.checkMate) {
                MainActivity.status.setText("Checkmate!");
            } else if (MainActivity.thisTurn.check) {
                MainActivity.status.setText("Check");
            } else {
                MainActivity.status.setText("");
            }

        } else {
            MainActivity.status.setText("Can't Undo");


        }
    }
}
