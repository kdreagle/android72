package com.example.kyle.android72;

/**
 * @author Matthew Skrobola, Kyle Reagle
 */
public class Bishop extends ChessPiece {

	/**
	 * Bishop chess piece
	 * @param color color of the bishop
	 * @param location location of the bishop
	 */
	public Bishop(String color, String location) {
		super(color,location);
	}
	
	/** 
	 * @return the correct notation for printing to the board
	 */
	public String toString() {
		return getColor()+"B";
	}
}
