package com.example.kyle.android72;

import android.media.Image;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Kyle on 12/5/2017.
 */

class CellClickListener implements View.OnClickListener {

    static ImageView src, dest;

    @Override
    public void onClick(View v) {
        ImageView piece = (ImageView) v;

        if (MainActivity.thisTurn.gameOver) return;

        if (MainActivity.src == null || MainActivity.src.equals(piece)) {
            MainActivity.src = piece;
        } else {

            MainActivity.dest = piece;
            if (MainActivity.thisTurn.playTurn(MainActivity.src.getTag().toString(),MainActivity.dest.getTag().toString())) {
                MainActivity.isDraw = false;
                MainActivity.status.setText("");

               MainActivity.updateBoard();



                src = MainActivity.src;
                dest = MainActivity.dest;

                MainActivity.thisTurn.analyzeBoard();

                if (MainActivity.thisTurn.whitesTurn) {
                    MainActivity.thisTurn.whitesTurn = false;
                    MainActivity.turn.setText("Black's turn");
                } else {
                    MainActivity.thisTurn.whitesTurn = true;
                    MainActivity.turn.setText("White's turn");
                }

                if (MainActivity.thisTurn.gameOver) {
                    MainActivity.status.setText("Checkmate!");
                    if (MainActivity.thisTurn.whitesTurn) {
                        MainActivity.turn.setText("Black Wins!");
                    } else {
                        MainActivity.turn.setText("White Wins!");
                    }
                    MainActivity.saveGame.setVisibility(View.VISIBLE);
                    MainActivity.undo.setVisibility(View.INVISIBLE);
                    MainActivity.autoMove.setVisibility(View.INVISIBLE);
                    MainActivity.draw.setVisibility(View.INVISIBLE);
                    MainActivity.resign.setVisibility(View.INVISIBLE);

                } else if (MainActivity.thisTurn.check) {
                    MainActivity.status.setText("Check");
                }

            } else {
                MainActivity.status.setText("Illegal Move Try Again"); // might take this out
            }
            MainActivity.src = null;
            MainActivity.dest = null;
        }
    }
}
