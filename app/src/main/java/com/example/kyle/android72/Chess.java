package com.example.kyle.android72;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Created by Kyle on 12/5/2017.
 */

public class Chess {

    Map<String, ChessPiece> board = new HashMap<String,ChessPiece>();

    boolean whitesTurn = true;

    boolean gameOver = false;

    private String lastMove = "";

    private String prevMove = "";

    private String blackKingLocation = "e8";

    private String whiteKingLocation = "e1";

    boolean check = false;

    boolean checkMate = false;

    private boolean whiteKingMoved = false;

    private boolean blackKingMoved = false;

    private boolean leftWhiteRookMoved = false;

    private boolean rightWhiteRookMoved = false;

    private boolean leftBlackRookMoved = false;

    private boolean rightBlackRookMoved = false;

    boolean undid = false;

    ChessPiece lastRemoved = null;

    private String lastMovedFrom;

    private BackupChess backupChess = new BackupChess();

    Map<String, Set<String>> randomMoveHolder;

    List<String> moveOrder = new ArrayList<>();

    void initializeBoard() {

        board.put("a8", new Rook("b","a8"));
        board.put("b8", new Knight("b","b8"));
        board.put("c8", new Bishop("b","c8"));
        board.put("d8", new Queen("b","d8"));
        board.put("e8", new King("b","e8"));
        board.put("f8", new Bishop("b","f8"));
        board.put("g8", new Knight("b","g8"));
        board.put("h8", new Rook("b","h8"));

        for (String file: new String[]{"a","b","c","d","e","f","g","h"}) {
            board.put(file+7+"", new Pawn("b",file+7+""));
            board.put(file+2+"", new Pawn("w",file+2+""));
        }

        board.put("a1", new Rook("w","a1"));
        board.put("b1", new Knight("w","b1"));
        board.put("c1", new Bishop("w","c1"));
        board.put("d1", new Queen("w","d1"));
        board.put("e1", new King("w","e1"));
        board.put("f1", new Bishop("w","f1"));
        board.put("g1", new Knight("w","g1"));
        board.put("h1", new Rook("w","h1"));
    }

    // Returns true if the move is valid
    boolean playTurn(String src, String dest) {
        BackupChess tempBUC = new BackupChess();
        initialUndoBackup(tempBUC);

        String s = src + " " + dest;
        if(s.indexOf("e1 g1") != -1 || s.indexOf("e1 c1") != -1 || s.indexOf("e8 g8") != -1 || s.indexOf("e8 c8") != -1) {
            if (checkCastle(dest)) {
                if (whitesTurn) lastMove = whiteKingLocation;
                else lastMove = blackKingLocation;
                return true;
            } else {
                return false;
            }
        }

        String color;
        if (whitesTurn)
            color = "w";
        else
            color = "b";

        if(!board.containsKey(src)) { // if there is no piece in that spot
            return false;
        } else if (!board.get(src).getColor().equals(color)) { // if the user tries to move the opposite color piece
            return false;
        } else {
            ChessPiece curPiece = board.get(src);
            Set<String> validMoves = getValidMoves(src, curPiece, color.charAt(0));
            if(validMoves.contains(dest)) {


                    ChessPiece chessPiece = null;
                    if(board.containsKey(dest)) chessPiece =  board.get(dest);
                    board.put(dest, curPiece);
                    board.remove(src);
                    String tempWhiteKingLocation = whiteKingLocation;
                    String tempBlackKingLocation = blackKingLocation;
                    if(color.charAt(0) == 'w' && src.equals(whiteKingLocation)) {
                        tempWhiteKingLocation = dest;
                    } else if(color.charAt(0) == 'b' && src.equals(blackKingLocation) ) {
                        tempBlackKingLocation = dest;
                    }
                    Set<String> opponentMoves;
                    if(color.charAt(0) == 'w') opponentMoves = getAllPlayerMoves(board.keySet(), 'b' );
                    else opponentMoves = getAllPlayerMoves(board.keySet(), 'w' );

                    board.put(src, board.get(dest));
                    if(chessPiece != null) {
                        board.put(dest, chessPiece);
                    } else {
                        board.remove(dest);
                    }

                    if(color.equals("w")) {
                        if(opponentMoves.contains(tempWhiteKingLocation)) {
                            return false;
                        }
                    } else {
                        if(opponentMoves.contains(tempBlackKingLocation)) {
                            return false;
                        }
                    }


                if(curPiece.toString().charAt(1) == 'P') {
                    if(dest.charAt(0) != src.charAt(0) && !board.containsKey(dest)) { // en passant check
                        char file = dest.charAt(0);
                        int rank = Integer.parseInt(dest.substring(1));
                        if(color.equals("w")) {
                            board.remove((char) (file) + Integer.toString(rank-1));
                        } else {
                            board.remove((char) (file) + Integer.toString(rank+1));
                        }
                    }
                }
                if(src.equals("a1") || dest.equals("a1")) leftWhiteRookMoved = true;
                if(src.equals("h1") || dest.equals("h1")) rightWhiteRookMoved = true;
                if(src.equals("a8") || dest.equals("a8")) rightBlackRookMoved = true;
                if(src.equals("h8") || dest.equals("h8")) leftBlackRookMoved = true;
                if(src.equals("e1")) whiteKingMoved = true;
                if(src.equals("e8")) blackKingMoved = true;
                curPiece.setLocation(dest);
                curPiece = promote(curPiece, s); //checks if this is a pawn and if it is promote it to queen
                if(board.containsKey(dest) )lastRemoved = board.get(dest);
                else lastRemoved = null;
                board.put(dest, curPiece);
                board.remove(src);

                if(curPiece.toString().charAt(1) == 'K') { //if the king moved changed update
                    if(curPiece.getColor() == "w") {
                        whiteKingLocation = dest;
                    } else {
                        blackKingLocation = dest;
                    }
                }
                lastMove = dest;
                lastMovedFrom = src;
                undid = false;
                copyUndoInfo(backupChess, tempBUC);
                moveOrder.add(src + dest);
                return true;
            } else {
                return false;
            }
        }
    }

    void analyzeBoard () {
        Set<String> moves = new HashSet<String>();
        Set<String> currentPlayerMoves = new HashSet<String>();

        check = false;
        checkMate = false;
        prevMove = lastMove;

        if (whitesTurn) {
            if(lastMove.equals(blackKingLocation)) {
                gameOver = true;
            } else {
                moves = getAllPlayerMoves(board.keySet(), 'w');
                if(moves.contains(blackKingLocation)) { // check if the last move put the enemy in check
                    check = true;
                    checkMate = checkCheckMate();
                }
            }
        } else {
            if(lastMove.equals(whiteKingLocation)) {
                gameOver = true;
            } else {
                moves = getAllPlayerMoves(board.keySet(), 'b');
                if(moves.contains(whiteKingLocation)) {  // check if the last move put the enemy in check
                    check = true;
                    Set<String> kingsMoves = getValidMoves(whiteKingLocation, board.get(whiteKingLocation),'w');
                    checkMate = checkCheckMate();
                }
            }
        }

        if(checkMate) {
            gameOver = true;
        } else if (check == true) {
            //System.out.println("Check");
        }
    }

    /** method to sort out which piece we are dealing with and call the appropriate method to find valid moves.
     * @param cur the current space the piece is on
     * @param chessPiece the current chess piece we are evaluating.
     * @param color current color
     * @return the set of strings representing valid moves
     */
    public Set<String> getValidMoves(String cur, ChessPiece chessPiece, char color){
        char piece = chessPiece.toString().charAt(1);
        Set<String> validMoves = new HashSet<String>();
        char file = cur.charAt(0);
        int rank = Integer.parseInt(cur.substring(1));
        if(piece == 'P') {
            getPawnMoves(validMoves, color, file, rank);
        } else if(piece == 'N') {
            getKnightMoves(validMoves, color, file, rank);
        } else if(piece == 'R') {
            getRookMoves(validMoves, color, file, rank);
        } else if(piece == 'B') {
            getBishopMoves(validMoves, color, file, rank);
        } else if(piece == 'Q') { // since queen has the same moves as Bishop + Rook
            getBishopMoves(validMoves, color, file, rank);
            getRookMoves(validMoves, color, file, rank);
        } else {
            getKingMoves(validMoves, color, file, rank);
        }
        return validMoves;
    }

    /** Adds the space to validSpace if either there is nothing in that current space or
     * the space is occupied by the enemy player's piece.
     * @param validMoves the Set of valid moves that the move will be added to if valid.
     * @param updated the move to check if it is valid
     * @param color current color
     * @return true if the space was empty, false otherwise
     */
    private boolean checkAndAdd(Set<String> validMoves , String updated, char color ) {
        if(board.containsKey(updated)) {
            if(board.get(updated).getColor().charAt(0) != color) {
                validMoves.add(updated);
            }
            return false;
        } else {
            validMoves.add(updated);
            return true;
        }
    }

    /** Checks if the input ChessPiece is a pawn and deserves to be promoted based on it's location
     * @param cur the ChessPiece we are checking
     * @return either a new Queen chessPiece if the pawn is promoted or the same ChessPiece that was inputed
     */
    private ChessPiece promote (ChessPiece cur, String input) {

        if(cur.toString().charAt(1) == 'P') {
            if(cur.toString().charAt(0) == 'b' && cur.getLocation().toString().charAt(1) == '1') {
                if(input.length() < 7) {
                    cur = new Queen("b", cur.getLocation());
                } else {
                    char type = input.charAt(6);
                    if(type == 'N') {
                        cur = new Knight("b", cur.getLocation());
                    } else if(type == 'R') {
                        cur = new Rook("b", cur.getLocation());
                    } else if(type == 'B') {
                        cur = new Bishop("b", cur.getLocation());
                    } else {
                        cur = new Queen("b", cur.getLocation());
                    }
                }

            } else if(cur.toString().charAt(0) == 'w' && cur.getLocation().toString().charAt(1) == '8') {
                cur = new Queen("w", cur.getLocation());
                if(input.length() < 7) {
                    cur = new Queen("w", cur.getLocation());
                } else {
                    char type = input.charAt(6);
                    if(type == 'N') {
                        cur = new Knight("w", cur.getLocation());
                    } else if(type == 'R') {
                        cur = new Rook("w", cur.getLocation());
                    } else if(type == 'B') {
                        cur = new Bishop("w", cur.getLocation());
                    } else {
                        cur = new Queen("w", cur.getLocation());
                    }
                }
            }
        }
        return cur;
    }

    /** Adds all of the valid moves for the Pawn piece to the validMoves Set.
     * @param validMoves the Set where the valid moves will be added to
     * @param color current color
     * @param file current file
     * @param rank current rank
     */
    private void getPawnMoves(Set<String> validMoves, char color, char file, int rank) {
        String updated = "";
        boolean hasMoved = true; // if this is the first time pawn is moving it can move 2 spaces
        if(color == 'w') {
            if(rank == 2)hasMoved = false;
            rank++;
        } else {
            if(rank == 7)hasMoved = false;
            rank--;
        }
        updated += file + Integer.toString(rank);


        if(!board.containsKey(updated) && checkAndAdd(validMoves, updated, color) && hasMoved == false){ // pawns can move twice there first turn
            if(color == 'w') {
                rank++;
            } else {
                rank--;
            }
            updated = file + Integer.toString(rank);
            if (!board.containsKey(updated)) checkAndAdd(validMoves, updated, color);
            if(color == 'w') {
                rank--;
            } else {
                rank++;
            }

        }
        if(color == 'w') { // reset from adding it earlier
            rank--;
        } else {
            rank++;
        }

        if(color == 'w') {
            if(rank + 1 < 9 && file -1 >= 'a' && board.containsKey((char) (file -1) + Integer.toString(rank+1)) ) checkAndAdd(validMoves, (char) (file -1) + Integer.toString(rank+1), 'w');
            if(rank + 1 < 9 && file +1 <= 'h' && board.containsKey((char) (file +1) + Integer.toString(rank+1))) checkAndAdd(validMoves, (char) (file +1) + Integer.toString(rank+1), 'w');
            if(rank + 1 < 9 && file -1 >= 'a' && !board.containsKey((char) (file -1) + Integer.toString(rank+1))) {
                if(prevMove.equals((char) (file -1) + Integer.toString(rank)) && rank + 2 == 7 && board.get((char) (file -1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
                    validMoves.add((char) (file -1) + Integer.toString(rank+1));
                }
            }
            if(rank + 1 < 9 && file +1 <= 'h' && !board.containsKey((char) (file +1) + Integer.toString(rank+1))) {
                if(prevMove.equals((char) (file +1) + Integer.toString(rank)) && rank + 2 == 7 && board.get((char) (file +1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
                    validMoves.add((char) (file +1) + Integer.toString(rank+1));
                }
            }

        } else {
            if(rank - 1 > 0 && file +1 <= 'h' && board.containsKey((char) (file +1) + Integer.toString(rank-1))) checkAndAdd(validMoves, (char) (file +1) + Integer.toString(rank-1), 'b');
            if(rank - 1 > 0 && file -1 <= 'h' && board.containsKey((char) (file -1) + Integer.toString(rank-1))) checkAndAdd(validMoves, (char) (file -1) + Integer.toString(rank-1), 'b');
            if(rank - 1 > 0 && file -1 >= 'a' && !board.containsKey((char) (file -1) + Integer.toString(rank-1))) {
                if(prevMove.equals((char) (file -1) + Integer.toString(rank)) && rank + 2 == 7 && board.get((char) (file -1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
                    validMoves.add((char) (file -1) + Integer.toString(rank-1));
                }
            }
            if(rank - 1 > 0 && file +1 <= 'h' && !board.containsKey((char) (file +1) + Integer.toString(rank-1))) {
                if(prevMove.equals((char) (file +1) + Integer.toString(rank)) && rank - 2 == 2 && board.get((char) (file +1) + Integer.toString(rank)).toString().charAt(1) == 'P') {
                    validMoves.add((char) (file +1) + Integer.toString(rank-1));
                }
            }
        }

    }

    /** Adds all of the valid moves for the Knight piece to the validMoves Set.
     * @param validMoves the Set where the valid moves will be added to
     * @param color current color
     * @param file current file
     * @param rank current rank
     */
    private void getKnightMoves(Set<String> validMoves, char color, char file, int rank) {
        List<String> movesToCheck = new ArrayList<String>();

        if(rank - 2 > 0 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank-2));
        if(rank - 1 > 0 && file -2 >= 'a') movesToCheck.add((char) (file -2) + Integer.toString(rank-1));
        if(rank - 2 > 0 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank-2));
        if(rank + 2 < 9 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank+2));
        if(rank + 1 < 9 && file -2 >= 'a') movesToCheck.add((char) (file -2) + Integer.toString(rank+1));
        if(rank - 1 > 0 && file +2 <= 'h') movesToCheck.add((char) (file +2) + Integer.toString(rank-1));
        if(rank + 1 < 9 && file +2 <= 'h') movesToCheck.add((char) (file +2) + Integer.toString(rank+1));
        if(rank + 2 < 9 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank+2));
        //now check if these moves are valid
        for(int i =0; i < movesToCheck.size(); i ++) {
            checkAndAdd(validMoves, movesToCheck.get(i), color);
        }

    }

    /** Adds all of the valid moves for the Rook piece to the validMoves Set.
     * @param validMoves the Set where the valid moves will be added to
     * @param color current color
     * @param file current file
     * @param rank current rank
     */
    private void getRookMoves(Set<String> validMoves, char color, char file, int rank) {
        List<String> movesToCheck = new ArrayList<String>();
        int tempRank = rank;
        char tempFile = file;
        tempRank--;
        while(tempRank > 0) {
            if(board.containsKey((char)file + Integer.toString(tempRank))) {
                movesToCheck.add((char)file + Integer.toString(tempRank));
                break;
            } else {
                movesToCheck.add((char)file + Integer.toString(tempRank));
            }
            tempRank--;
        }
        tempRank = rank;
        tempRank++;
        while(tempRank < 9) {
            if(board.containsKey((char)file + Integer.toString(tempRank))) {
                movesToCheck.add((char)file + Integer.toString(tempRank));
                break;
            } else {
                movesToCheck.add((char)file + Integer.toString(tempRank));
            }
            tempRank++;
        }
        tempFile--;
        while(tempFile >= 'a') {
            if(board.containsKey((char)tempFile + Integer.toString(rank))) {
                movesToCheck.add((char)tempFile + Integer.toString(rank));
                break;
            } else {
                movesToCheck.add((char)tempFile + Integer.toString(rank));
            }
            tempFile--;
        }
        tempFile = file;
        tempFile++;
        while(tempFile <= 'h') {
            if(board.containsKey((char)tempFile + Integer.toString(rank))) {
                movesToCheck.add((char)tempFile + Integer.toString(rank));
                break;
            } else {
                movesToCheck.add((char)tempFile + Integer.toString(rank));
            }
            tempFile++;
        }
        for(int i =0; i < movesToCheck.size(); i ++) {
            checkAndAdd(validMoves, movesToCheck.get(i), color);
        }
    }

    /**
     * Adds all of the validMoves for a Bishop to the validMoves Set.
     * @param validMoves the set where valid moves will be added to
     * @param color current color
     * @param file current file
     * @param rank current rank
     */
    private void getBishopMoves(Set<String> validMoves, char color, char file, int rank) {
        List<String> movesToCheck = new ArrayList<String>();
        int tempRank = rank;
        char tempFile = file;
        tempRank--;
        tempFile--;
        while(tempRank > 0 && tempFile >= 'a') {
            if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
                break;
            } else {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
            }
            tempRank--;
            tempFile--;
        }
        tempRank = rank;
        tempFile = file;
        tempRank++;
        tempFile++;
        while(tempRank < 9 && tempFile <= 'h') {
            if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
                break;
            } else {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
            }
            tempRank++;
            tempFile++;
        }
        tempFile = file;
        tempRank = rank;
        tempFile--;
        tempRank++;
        while(tempFile >= 'a' && tempRank < 9) {
            if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
                break;
            } else {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
            }
            tempFile--;
            tempRank++;
        }
        tempFile = file;
        tempRank = rank;
        tempFile++;
        tempRank--;
        while(tempFile <= 'h' && tempRank > 0) {
            if(board.containsKey((char)tempFile + Integer.toString(tempRank))) {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
                break;
            } else {
                movesToCheck.add((char)tempFile + Integer.toString(tempRank));
            }
            tempFile++;
            tempRank--;
        }
        for(int i =0; i < movesToCheck.size(); i ++) {
            checkAndAdd(validMoves, movesToCheck.get(i), color);
        }

    }

    /** Adds all of the valid moves for the King peice into the Set validMoves
     * @param validMoves the set containing all of the valid moves
     * @param color current color piece
     * @param file current file
     * @param rank current rank
     */
    private void getKingMoves(Set<String> validMoves, char color, char file, int rank) {
        List<String> movesToCheck = new ArrayList<String>();

        if(rank - 1 > 0) movesToCheck.add((char) (file) + Integer.toString(rank-1));
        if(rank + 1 < 9) movesToCheck.add((char) (file) + Integer.toString(rank+1));
        if(file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank));
        if(file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank));
        if(rank + 1 < 9 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank+1));
        if(rank - 1 > 0 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank-1));
        if(rank + 1 < 9 && file +1 <= 'h') movesToCheck.add((char) (file +1) + Integer.toString(rank+1));
        if(rank - 1 > 0 && file -1 >= 'a') movesToCheck.add((char) (file -1) + Integer.toString(rank-1));
        //now check if these moves are valid
        for(int i =0; i < movesToCheck.size(); i ++) {

            checkAndAdd(validMoves, movesToCheck.get(i), color);
        }
    }

    /**
     * gathers all potential moves of the player
     * @param boardKeys - the keySet of board
     * @param color - the color of the player which moves the user wants to obtain
     * @return a set of Strings containing all potential moves
     */
    public Set<String> getAllPlayerMoves(Set<String> boardKeys, char color){
        Set<String> validMoves = new HashSet<>();
        for(String cur : boardKeys) {
            ChessPiece chessPiece = board.get(cur);
            if(chessPiece.toString().charAt(0) == color) {
                char piece = chessPiece.toString().charAt(1);
                char file = cur.charAt(0);
                int rank = Integer.parseInt(cur.substring(1));
                if(piece == 'P') {
                    getPawnMoves(validMoves, color, file, rank);
                } else if(piece == 'N') {
                    getKnightMoves(validMoves, color, file, rank);
                } else if(piece == 'R') {
                    getRookMoves(validMoves, color, file, rank);
                } else if(piece == 'B') {
                    getBishopMoves(validMoves, color, file, rank);
                } else if(piece == 'Q') { // since queen has the same moves as Bishop + Rook
                    getBishopMoves(validMoves, color, file, rank);
                    getRookMoves(validMoves, color, file, rank);
                } else {
                    getKingMoves(validMoves, color, file, rank);
                }

            }
        }
        return validMoves;
    }

    /**
     * Checks if castling can occur
     * @param dest the destination of the King
     * @return boolean true if it can false otherwise
     */
    public boolean checkCastle(String dest) {

        if(!check) {
            if(whitesTurn) {
                if(!whiteKingMoved) {
                    Set<String> opponentMoves = getAllPlayerMoves(board.keySet(), 'b');
                    if(dest.equals("g1")) {
                        if(!board.containsKey("f1") && !board.containsKey("g1") && !rightWhiteRookMoved && !opponentMoves.contains("f1") && !opponentMoves.contains("g1")) {
                            board.put("f1", board.get("h1"));
                            board.put("g1", board.get("e1"));
                            board.remove("h1");
                            board.remove("e1");
                            whiteKingMoved = true;
                            rightWhiteRookMoved = true;
                            whiteKingLocation = "g1";
                            return true;
                        }
                    } else {
                        if(!board.containsKey("d1") && !board.containsKey("c1") && !leftWhiteRookMoved && !opponentMoves.contains("c1") && !opponentMoves.contains("d1")) {
                            board.put("d1", board.get("a1"));
                            board.put("c1", board.get("e1"));
                            board.remove("a1");
                            board.remove("e1");
                            whiteKingMoved = true;
                            leftWhiteRookMoved = true;
                            whiteKingLocation = "c1";
                            return true;
                        }
                    }

                }
            } else {
                if(!blackKingMoved) {
                    Set<String> opponentMoves = getAllPlayerMoves(board.keySet(), 'w');
                    if(dest.equals("g8")) {
                        if(!board.containsKey("f8") && !board.containsKey("g8") && !rightBlackRookMoved && !opponentMoves.contains("f8") && !opponentMoves.contains("g8")) {
                            board.put("f8", board.get("h8"));
                            board.put("g8", board.get("e8"));
                            board.remove("h8");
                            board.remove("e8");
                            blackKingMoved = true;
                            rightBlackRookMoved = true;
                            blackKingLocation = "g8";
                            return true;
                        }
                    } else {
                        if(!board.containsKey("d8") && !board.containsKey("c8") && !leftBlackRookMoved && !opponentMoves.contains("c8") && !opponentMoves.contains("d8")) {
                            board.put("d8", board.get("a8"));
                            board.put("c8", board.get("e8"));
                            board.remove("a8");
                            board.remove("e8");
                            blackKingMoved = true;
                            leftBlackRookMoved = true;
                            blackKingLocation = "c8";
                            return true;
                        }
                    }

                }
            }
        }
        return false;

    }

    public boolean undo(){
        if(gameOver)return false;
        if(undid)return false;
        if(lastMove.equals("")) return false;
        board.put(lastMovedFrom, board.get(lastMove));
        if(lastRemoved != null)  board.put(lastMove, lastRemoved);
        else board.remove(lastMove);
        undid = true;
        moveOrder.remove(moveOrder.size()-1);
        return true;
    }

    public void copyUndoInfo(BackupChess a, BackupChess b){
        a.check = b.check;
        a.checkMate = b.checkMate;
        a.gameOver = b.gameOver;
        a.whitesTurn = b.whitesTurn;
        a.blackKingLocation = b.blackKingLocation;
        a.whiteKingLocation = b.whiteKingLocation;
        a.blackKingMoved = b.blackKingMoved;
        a.whiteKingMoved = b.whiteKingMoved;
        a.lastMove = b.lastMove;
        a.leftBlackRookMoved = b.leftBlackRookMoved;
        a.leftWhiteRookMoved = b.leftWhiteRookMoved;
        a.rightBlackRookMoved = b.rightBlackRookMoved;
        a.rightWhiteRookMoved = b.rightWhiteRookMoved;
        a.prevMove = b.prevMove;
    }

    public void initialUndoBackup(BackupChess a){
        a.check = check;
        a.checkMate = checkMate;
        a.gameOver = gameOver;
        a.whitesTurn = whitesTurn;
        a.blackKingLocation = blackKingLocation;
        a.whiteKingLocation = whiteKingLocation;
        a.blackKingMoved = blackKingMoved;
        a.whiteKingMoved = whiteKingMoved;
        a.lastMove = lastMove;
        a.leftBlackRookMoved = leftBlackRookMoved;
        a.leftWhiteRookMoved = leftWhiteRookMoved;
        a.rightBlackRookMoved = rightBlackRookMoved;
        a.rightWhiteRookMoved = rightWhiteRookMoved;
        a.prevMove = prevMove;
    }

    public void previousToCurrent(){
        check = backupChess.check;
        checkMate = backupChess.checkMate;
        gameOver = backupChess.gameOver;
        whitesTurn = backupChess.whitesTurn;
        blackKingLocation = backupChess.blackKingLocation;
        whiteKingLocation = backupChess.whiteKingLocation;
        blackKingMoved = backupChess.blackKingMoved;
        whiteKingMoved = backupChess.whiteKingMoved;
        lastMove = backupChess.lastMove;
        leftBlackRookMoved = backupChess.leftBlackRookMoved;
        leftWhiteRookMoved = backupChess.leftWhiteRookMoved;
        rightBlackRookMoved = backupChess.rightBlackRookMoved;
        rightWhiteRookMoved = backupChess.rightWhiteRookMoved;
        prevMove = backupChess.prevMove;
    }


    public boolean autoMove() {
        if (gameOver) return false;
        char color = 'w';
        if (!whitesTurn) color = 'b';
        getAllPlayerMovesExtra(board.keySet(), color);
        ArrayList<String> sources = new ArrayList<>(randomMoveHolder.keySet());

        int src = new Random().nextInt(sources.size());
        int dest = new Random().nextInt(randomMoveHolder.get(sources.get(src)).size());
        ArrayList<String> dests = new ArrayList<>(randomMoveHolder.get(sources.get(src)));
        int buffer =0;
        if(randomMoveHolder.isEmpty())return false;
        while (!playTurn(sources.get(src), dests.get(dest)) && buffer < 50){

            getAllPlayerMovesExtra(board.keySet(), color);
            sources = new ArrayList<>(randomMoveHolder.keySet());
            src = new Random().nextInt(sources.size());
            dest = new Random().nextInt(randomMoveHolder.get(sources.get(src)).size());
            dests = new ArrayList<>(randomMoveHolder.get(sources.get(src)));
            buffer++;
        }
        return(buffer < 50);

    }

    public boolean checkCheckMate() {
        char color = 'b';
        if (!whitesTurn) color = 'w';
        getAllPlayerMovesExtra(board.keySet(), color);
        ArrayList<String> sources = new ArrayList<>(randomMoveHolder.keySet());
        for(String src : randomMoveHolder.keySet()){
            Set<String> a = randomMoveHolder.get(src);
                if(!a.isEmpty()) {
                for (String dest : a) {
                    ChessPiece chessPiece = null;
                    ChessPiece curPiece = board.get(src);
                    if(board.containsKey(dest)) chessPiece =  board.get(dest);
                    board.put(dest, curPiece);
                    board.remove(src);
                    String tempWhiteKingLocation = whiteKingLocation;
                    String tempBlackKingLocation = blackKingLocation;
                    if(color == 'w' && src.equals(whiteKingLocation)) {
                        tempWhiteKingLocation = dest;
                    } else if(color == 'b' && src.equals(blackKingLocation) ) {
                        tempBlackKingLocation = dest;
                    }
                    Set<String> opponentMoves;
                    if(color == 'w') opponentMoves = getAllPlayerMoves(board.keySet(), 'b' );
                    else opponentMoves = getAllPlayerMoves(board.keySet(), 'w' );

                    board.put(src, board.get(dest));
                    if(chessPiece != null) {
                        board.put(dest, chessPiece);
                    } else {
                        board.remove(dest);
                    }

                    if(color == 'w') {
                        if(!opponentMoves.contains(tempWhiteKingLocation)) {
                            return false;
                        }
                    } else {
                        if(!opponentMoves.contains(tempBlackKingLocation)) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;


    }
    public Set<String> getAllPlayerMovesExtra(Set<String> boardKeys, char color){
        randomMoveHolder = new HashMap<>();
        Set<String> validMoves = new HashSet<>();
        for(String cur : boardKeys) {
            ChessPiece chessPiece = board.get(cur);
            if(chessPiece.toString().charAt(0) == color) {
                char piece = chessPiece.toString().charAt(1);
                char file = cur.charAt(0);
                int rank = Integer.parseInt(cur.substring(1));
                if(piece == 'P') {
                    getPawnMoves(validMoves, color, file, rank);
                } else if(piece == 'N') {
                    getKnightMoves(validMoves, color, file, rank);
                } else if(piece == 'R') {
                    getRookMoves(validMoves, color, file, rank);
                } else if(piece == 'B') {
                    getBishopMoves(validMoves, color, file, rank);
                } else if(piece == 'Q') { // since queen has the same moves as Bishop + Rook
                    getBishopMoves(validMoves, color, file, rank);
                    getRookMoves(validMoves, color, file, rank);
                } else {
                    getKingMoves(validMoves, color, file, rank);
                }
                if(!validMoves.isEmpty()){
                    Set<String> tempSet = new HashSet<>(validMoves);
                    randomMoveHolder.put("" + file + rank, tempSet);
                }
                validMoves = new HashSet<>();

            }
        }
        for(String sources : randomMoveHolder.keySet()){
            validMoves.addAll(randomMoveHolder.get(sources));
        }
        return validMoves;
    }






}
