package com.example.kyle.android72;

/**
 * @author Matthew Skrobola, Kyle Reagle
 *
 */
public class Rook extends ChessPiece {
	
	/** 
	 * Rook chess piece
	 * @param color color of the rook
	 * @param location location of rook
	 */
	public Rook(String color, String location) {
		super(color,location);
	}
	
	/** 
	 * @return the correct notation for printing to the board
	 */
	public String toString() {
		return getColor()+"R";
	}
}
