package com.example.kyle.android72;

/**
 * @author Matthew Skrobola, Kyle Reagle
 *
 */
public class Pawn extends ChessPiece {

	/** 
	 * Pawn chess piece
	 * @param color color of the pawn
	 * @param location location of the pawn
	 */
	public Pawn(String color, String location) {
		super(color,location);
	}
	
	/** 
	 * @return the correct notation for printing to the board
	 */
	public String toString() {
		return getColor()+"P";
	}
}
