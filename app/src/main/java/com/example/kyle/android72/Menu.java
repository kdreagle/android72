package com.example.kyle.android72;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

/**
 * Created by mattskrobola on 12/11/17.
 */

public class Menu extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.menu);
    }

    public void startGame(View v){
        Intent i = new Intent(Menu.this, MainActivity.class);
        startActivity(i);
    }

    public void viewGames(View v){
        Intent i = new Intent(Menu.this, ViewGames.class);
        startActivity(i);
    }

    public void writeToFile(ArrayList<SavedGame> songList) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("SavedGames"));
        objectOutputStream.writeObject(songList);
        objectOutputStream.close();
    }
}
