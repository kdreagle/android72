package com.example.kyle.android72;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by mattskrobola on 12/12/17.
 */

public class SavedGame implements Serializable{

    List<String> moves;

    String title;

    Calendar addedDate;

    public SavedGame(List<String> moves, String title){

        this.title = title;
        this.moves = moves;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        this.addedDate = cal;
    }

    public String toString(){
        return title + " - " + addedDate.getTime().toString();
    }
}
