package com.example.kyle.android72;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mattskrobola on 12/10/17.
 */

public class BackupChess {

    boolean whitesTurn;

    boolean gameOver;

    String lastMove;

    String prevMove;

    String blackKingLocation;

    String whiteKingLocation;

    boolean check;

    boolean checkMate;

    boolean whiteKingMoved;

    boolean blackKingMoved;

    boolean leftWhiteRookMoved;

    boolean rightWhiteRookMoved;

    boolean leftBlackRookMoved;

    boolean rightBlackRookMoved;

}
