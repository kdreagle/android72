package com.example.kyle.android72;

/**
 * @author Matthew Skrobola, Kyle Reagle
 */
public class King extends ChessPiece {
	
	/** 
	 * King chess piece
	 * @param color color of the king
	 * @param location location of the king
	 */
	public King(String color, String location) {
		super(color,location);
	}
	
	/** 
	 * @return the correct notation for printing to the board
	 */
	public String toString() {
		return getColor()+"K";
	}
}
