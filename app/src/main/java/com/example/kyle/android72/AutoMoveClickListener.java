package com.example.kyle.android72;

import android.view.View;

/**
 * Created by mattskrobola on 12/10/17.
 */

class AutoMoveClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        if(MainActivity.thisTurn.autoMove()){

            MainActivity.updateBoard();
            MainActivity.thisTurn.analyzeBoard();
            if (MainActivity.thisTurn.whitesTurn) {
                MainActivity.thisTurn.whitesTurn = false;
                MainActivity.turn.setText("Black's turn");
            } else {
                MainActivity.thisTurn.whitesTurn = true;
                MainActivity.turn.setText("White's turn");
            }

            if (MainActivity.thisTurn.checkMate) {
                if (MainActivity.thisTurn.whitesTurn) {
                    MainActivity.turn.setText("Black Wins!");
                } else {
                    MainActivity.turn.setText("White Wins!");
                }
                MainActivity.status.setText("Checkmate!");
                MainActivity.saveGame.setVisibility(View.VISIBLE);
                MainActivity.undo.setVisibility(View.INVISIBLE);
                MainActivity.autoMove.setVisibility(View.INVISIBLE);
            } else if (MainActivity.thisTurn.check) {
                MainActivity.status.setText("Check");
            } else {
                MainActivity.status.setText("");
            }

        } else {
            if (MainActivity.thisTurn.checkMate)
                MainActivity.status.setText("Checkmate!");
            else
                MainActivity.status.setText("Unable to find Move");
        }
    }
}
