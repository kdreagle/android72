package com.example.kyle.android72;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mattskrobola on 12/12/17.
 */

public class PlayOldGame extends AppCompatActivity {


    static HashMap<String, ImageView> cells;

    static GridLayout board;

    static TextView status;

    static TextView turn;

    static Chess thisTurn;

    static ImageView src, dest;

    static Button nextMove;

    static Button goBack;

    static Drawable undoSRC;

    static int globalCounter;

    static ArrayList<String> moves = new ArrayList<>();

    static Drawable bPawn;
    static Drawable wPawn;
    static Drawable bKing;
    static Drawable bQueen;
    static Drawable bRook;
    static Drawable bKnight;
    static Drawable bBishop;
    static Drawable wQueen;
    static Drawable wRook;
    static Drawable wKnight;
    static Drawable wBishop;
    static Drawable wKing;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalCounter = 0;
        setContentView(R.layout.play_old_game);
        initializeBoard();

    }

    public void goHome(View v){
        globalCounter = 0;
        finish();
    }



    void initializeBoard() {



        thisTurn = new Chess();

        thisTurn.initializeBoard();

        goBack = (Button) findViewById(R.id.goBack);

        nextMove = (Button) findViewById(R.id.nextMove);

        board = (GridLayout) findViewById(R.id.board);

        status = (TextView) findViewById(R.id.status);

        turn = (TextView) findViewById(R.id.turn);

        Intent intent = getIntent();

        moves = intent.getStringArrayListExtra("moves");


        nextMove.setOnClickListener(new PlayOldGameNextMoveClickListener());

        ImageView cell;

        cells = new HashMap<String, ImageView>();

        for (int rank = 8; rank > 0 ; rank--) {
            for (int file = 'a'; file <= 'h'; file++) {

                String position = "" + (char) file + rank;
                cell = new ImageView(this);


                if (rank == 7) {
                    cell.setImageResource(R.drawable.ic_bp);
                    bPawn = cell.getDrawable();
                } else if (rank == 2) {
                    cell.setImageResource(R.drawable.ic_wp);
                    wPawn = cell.getDrawable();
                } else if (position.equals("a8") || position.equals("h8")) {
                    cell.setImageResource(R.drawable.ic_br);
                    bRook = cell.getDrawable();
                } else if (position.equals("b8") || position.equals("g8")) {
                    cell.setImageResource(R.drawable.ic_bn);
                    bKnight = cell.getDrawable();
                } else if (position.equals("c8") || position.equals("f8")) {
                    cell.setImageResource(R.drawable.ic_bb);
                    bBishop  = cell.getDrawable();
                } else if (position.equals("d8")) {
                    cell.setImageResource(R.drawable.ic_bq);
                    bQueen  = cell.getDrawable();
                } else if (position.equals("e8")) {
                    cell.setImageResource(R.drawable.ic_bk);
                    bKing  = cell.getDrawable();
                } else if (position.equals("a1") || position.equals("h1")) {
                    cell.setImageResource(R.drawable.ic_wr);
                    wRook  = cell.getDrawable();
                } else if (position.equals("b1") || position.equals("g1")) {
                    cell.setImageResource(R.drawable.ic_wn);
                    wKnight  = cell.getDrawable();
                } else if (position.equals("c1") || position.equals("f1")){
                    cell.setImageResource(R.drawable.ic_wb);
                    wBishop  = cell.getDrawable();
                } else if (position.equals("d1")){
                    cell.setImageResource(R.drawable.ic_wq);
                    wQueen  = cell.getDrawable();
                } else if (position.equals("e1")) {
                    cell.setImageResource(R.drawable.ic_wk);
                    wKing  = cell.getDrawable();
                }


                cell.setMinimumWidth(92);
                cell.setMinimumHeight(92);


                cell.setTag(position);
                cells.put(position, cell);


                cell.setOnClickListener(new CellClickListener());



                board.addView(cell);
            }
        }
        ImageView c = new ImageView(this);
        c.setImageResource(R.drawable.ic_bb);
    }
    public static void updateBoard() {

        Map<String, ChessPiece> curBoard = thisTurn.board;

        ImageView cell;
        HashMap<String, ImageView> cells = PlayOldGame.cells;

        for (String position : cells.keySet()) {
            cell = cells.get(position);
            if (curBoard.containsKey(position)) {
                ChessPiece chessPiece = curBoard.get(position);
                char color = chessPiece.toString().charAt(0);
                char piece = chessPiece.toString().charAt(1);
                if (piece == 'P' && color == 'b')
                    cell.setImageDrawable(PlayOldGame.bPawn);
                else if (piece == 'P' && color == 'w')
                    cell.setImageDrawable(PlayOldGame.wPawn);
                else if (piece == 'R' && color == 'b')
                    cell.setImageDrawable(PlayOldGame.bRook);
                else if (piece == 'N' && color == 'b')
                    cell.setImageDrawable(PlayOldGame.bKnight);
                else if (piece == 'B' && color == 'b')
                    cell.setImageDrawable(PlayOldGame.bBishop);
                else if (piece == 'Q' && color == 'b')
                    cell.setImageDrawable(PlayOldGame.bQueen);
                else if (piece == 'K' && color == 'b')
                    cell.setImageDrawable(PlayOldGame.bKing);
                else if (piece == 'R' && color == 'w')
                    cell.setImageDrawable(PlayOldGame.wRook);
                else if (piece == 'N' && color == 'w')
                    cell.setImageDrawable(PlayOldGame.wKnight);
                else if (piece == 'B' && color == 'w')
                    cell.setImageDrawable(PlayOldGame.wBishop);
                else if (piece == 'Q' && color == 'w')
                    cell.setImageDrawable(PlayOldGame.wQueen);
                else if (piece == 'K' && color == 'w')
                    cell.setImageDrawable(PlayOldGame.wKing);
            } else {
                cell.setImageDrawable(null);
            }
        }
    }
}

