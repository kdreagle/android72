package com.example.kyle.android72;

import android.view.View;

/**
 * Created by mattskrobola on 12/12/17.
 */

public class PlayOldGameNextMoveClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
        if(PlayOldGame.globalCounter >= PlayOldGame.moves.size())return;
        String input = PlayOldGame.moves.get(PlayOldGame.globalCounter);
        String src = input.substring(0,2);
        String dest = input.substring(2);
        if(src.equals("ww")){
            PlayOldGame.turn.setText("White Wins!");
            PlayOldGame.globalCounter++;
        } else if (src.equals("bw")){
            PlayOldGame.turn.setText("Black Wins!");
            PlayOldGame.globalCounter++;
        } else if (src.equals("dr")){
            PlayOldGame.turn.setText("Draw!");
            PlayOldGame.globalCounter++;
        } else if(PlayOldGame.thisTurn.playTurn(src, dest)){

            PlayOldGame.updateBoard();
            PlayOldGame.thisTurn.analyzeBoard();
            if (PlayOldGame.thisTurn.whitesTurn) {
                PlayOldGame.thisTurn.whitesTurn = false;
                PlayOldGame.turn.setText("Black's turn");
            } else {
                PlayOldGame.thisTurn.whitesTurn = true;
                PlayOldGame.turn.setText("White's turn");
            }

            if (PlayOldGame.thisTurn.checkMate) {
                if (PlayOldGame.thisTurn.whitesTurn) {
                    PlayOldGame.turn.setText("Black Wins!");
                } else {
                    PlayOldGame.turn.setText("White Wins!");
                }
                PlayOldGame.status.setText("Checkmate!");
            } else if (PlayOldGame.thisTurn.check) {
                PlayOldGame.status.setText("Check");
            } else {
                PlayOldGame.status.setText("");
            }
            PlayOldGame.globalCounter++;

        } else {
            if (PlayOldGame.thisTurn.checkMate)
                PlayOldGame.status.setText("Checkmate!");
            else
                PlayOldGame.status.setText("Error with playback");
        }
    }
}
