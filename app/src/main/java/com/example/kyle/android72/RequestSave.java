package com.example.kyle.android72;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by mattskrobola on 12/12/17.
 */

public class RequestSave extends AppCompatActivity implements Serializable {
    static Button confirm;

    static EditText title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_save);

        title = (EditText) findViewById(R.id.title);



        confirm = (Button) findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent intent = getIntent();
                SavedGame savedGame = new SavedGame(intent.getStringArrayListExtra("moves"),
                        title.getText().toString());


                ArrayList<SavedGame> savedGames = new ArrayList<>();
                ObjectInputStream input;
                String filename = "SavedGames";

                try {
                    input = new ObjectInputStream(new FileInputStream(new File(new File(getFilesDir(),"")+File.separator+filename)));
                    savedGames = (ArrayList<SavedGame>) input.readObject();
                    Log.v("serialization","list="+savedGames.toString());
                    input.close();
                } catch (StreamCorruptedException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                savedGames.add(savedGame);
                ObjectOutput out = null;
                try {
                    out = new ObjectOutputStream(new FileOutputStream(new File(getFilesDir(),"")+File.separator+filename));
                    out.writeObject(savedGames);
                    out.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                finish();

            }
        });
    }

}
